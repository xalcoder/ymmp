import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode


class ItemsEditor(private val maxLayerForWholeMovie: Int) {

    fun editFrameAndLayer(partNodeRoots: List<JsonNode>) {
        val itemsNodes = partNodeRoots.map { nodeRoot -> nodeRoot.get("Timeline").get("Items") }
        alignFrames(itemsNodes)
        itemsNodes.forEach { items -> alignLayer(items) }
        alignGroups(itemsNodes)
    }

    private fun alignGroups(itemsNodes: List<JsonNode>) {
        for(i in itemsNodes.indices){
            val items = itemsNodes[i]
            items.forEach { item ->
                //グループ番号0はグループ無しと思われる ファイルごとに1000ずつ領域をずらして重複を回避
                val groupNum = item.get("Group").asInt()
                if(groupNum != 0){
                    val alignedGroupNum = i * 1000 + groupNum
                    (item as ObjectNode).put("Group", alignedGroupNum)
                }
            }
        }
    }

    private fun alignFrames(itemsNodes: List<JsonNode>){
        for(i in itemsNodes.indices){
            if(i == 0){
                continue
            } else {
                val beforePartEndFrame = calcEndFrame(itemsNodes[i-1])//process in order
                alignFrame(itemsNodes[i], beforePartEndFrame)
            }
        }
    }

    private fun calcEndFrame(items: JsonNode): Int {
        return items.maxOf { item -> item.get("Frame").asInt() + item.get("Length").asInt() }
    }

    private fun alignFrame(items: JsonNode, addedFrame: Int) {
        for(item in items){
            val originalFrame = item.get("Frame").asInt()
            val alignedFrame = originalFrame + addedFrame
            (item as ObjectNode).put("Frame", alignedFrame)
        }
    }

    private fun alignLayer(items: JsonNode){
        for(item in items){
            val originalLayer = item.get("Layer").asInt()
            (item as ObjectNode).put("Layer", originalLayer + maxLayerForWholeMovie)
        }
    }

}