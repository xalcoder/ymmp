import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import java.io.File

class Combiner(projectName: String) {

    private val directoryPath = "S:\\YMM\\project\\${projectName}\\"
    private val mapper = ObjectMapper()
    private val EXT = ".ymmp"
    private val combinedFile = File(directoryPath+"combined"+EXT)
    private val maxLayerForWholeMovie = 5

    fun exec(){
        //TODO ファイルが更新されたら自動的にやりたい
        val partNodeRoots = readPartFiles()
        val baseNodeRoot =
            if (combinedFile.exists()) mapper.readTree(combinedFile)
            else                       partNodeRoots[0]

        ItemsEditor(maxLayerForWholeMovie).editFrameAndLayer(partNodeRoots)
        val combinedItems = combineItems(baseNodeRoot, partNodeRoots)
        (baseNodeRoot.get("Timeline") as ObjectNode).replace("Items", combinedItems)
        // JsonNode is immutable. ObjectNode is mutable and JsonNode can be converted to ObjectNode.

        val formattedJsonText = JsonFormatter().format(baseNodeRoot)
        combinedFile.writeText(formattedJsonText)

        //TODO　２．他のファイルのcharactersを置き換えるのもやりたい
    }

    private fun readPartFiles(): List<JsonNode>{
        val partFileNames = File(directoryPath).walk()
            .map { file -> file.name}
            .filter { fileName -> fileName.matches(Regex("\\d+\\.ymmp")) }
            .toList()
        return partFileNames
            .sorted()
            .map { fileName -> File(directoryPath + fileName) }
            .map { file -> mapper.readTree(file) }
    }

    private fun combineItems(baseNodeRoot: JsonNode, alignedPartNodeRoots: List<JsonNode>): ArrayNode {

        val combinedItems = mapper.createArrayNode()
        baseNodeRoot.get("Timeline").get("Items")
            .filter { item -> item.get("Layer").asInt() <= maxLayerForWholeMovie }
            .forEach { item -> combinedItems.add(item) }

        alignedPartNodeRoots
            .map { nodeRoot -> nodeRoot.get("Timeline").get("Items") }
            .flatMap { items -> items.asIterable() }
            .forEach { item -> combinedItems.add(item) }

        return combinedItems
    }

}