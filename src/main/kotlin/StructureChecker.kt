import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File

class StructureChecker {
    private val mapper = ObjectMapper()

    private var indentNum = 0

    fun exec(){
        val pathName = "S:\\YMM\\project\\shrineUdaSau.ymmp"
        val ymmpFile = File(pathName)
        val rootNode = mapper.readTree(ymmpFile)
        val fields = rootNode.fields()

        for(nodeEntry in fields){
            recurse(nodeEntry.key, nodeEntry.value)
        }
    }

    private fun recurse(name: String, node: JsonNode){
        println(indent() + name +":"+ node.size())
        indentNum++
        if(node.isArray){
            for(i in 0 until node.size()){
                println(indent() + node.get(i))
            }
        } else {
            for (nodeEntry in node.fields()){
                recurse(nodeEntry.key, nodeEntry.value)
            }
        }
        indentNum--
    }

    private fun indent(): String {
        val sb = StringBuilder()
        repeat(indentNum){
            sb.append("\t")
        }
        return sb.toString()
    }

}